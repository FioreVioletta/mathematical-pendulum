clear;

m = 0.1;                % masa punktu materialnego
l = 1.5;                % dlugosc linki wahad≥a
g = 9.81;               % przyspieszenie ziemskie
t = 50;                 % czas symulacji
k = 0.05;               % wspolczynnik tlumienia
dt = 0.01;              % ktok czasowy
dtau= dt * sqrt(g/l);   % zmienna do sparametryzwoania růwnania
N = t/dt;               % ca≥kowita liczba iteracji
eps = 0;                % si≥a wymuszajĪca               
w = 1;                  % czÍsto∂ś wymuszajĪca

t = 0:N;                % parametr czasowy

theta = zeros(N,1);     % macierz z warto∂ciamy po≥oŅenia
omega = zeros(N,1);     % macierz z warto∂ciamy prÍdko∂ci
alpha = zeros(N,1);     % macierz z warto∂ciamy przyspieszenia
T = zeros(N,1);         % macierz z warto∂ciamy czasu

theta(1,:) = pi/4;      % pozycja poczĪtkowa 
omega(1,:) = 0;         % poczĪtkowa prÍdko∂ś kĪtowa
alpha(1,:) = 0;         % poczĪtkowe przyspieszenie kĪtowe
T(1,:) = 0;             % poczĪtkowy czas 

for n=1:N               % g≥ůwna pÍtla do liczenia růwnaŮ růŅnickowych 
 
    T(n+1,:) = T(n,:) + dtau;  % zmiana czasu 
    theta(n+1,:) = theta(n,:)+ (omega(n,:)*dtau); % warto∂ś kĪta
    omega(n+1,:) = omega(n,:)+ (alpha(n,:)*dtau); % warto∂ś czÍstosci ko≥owej
    alpha(n+1,1) = -(g/l)*sin(theta(n+1,1)) - ((k/m)*omega(n+1,1)) + (eps/(m*l))*cos(w*sqrt(l/g)*T(n+1,:)); % przyspieszenie wahad≥a matematycznego
    
end

% tworzenie trzech wykresůw pokazujĪcych zaleŅno∂ci kĪta odchylenia, 
% czÍsto∂ci ko≥owej oraz przyspieszenia kĪtowego od czasu
figure;
subplot(3,1,1);
plot(t,theta'.*(180/pi),'Color', [ 0.8500 0.3250 0.0980], 'LineWidth', 2);
ylabel('Po≥oŅenie (rad)');
title('Funkcja czasu');
subplot(3,1,2);
plot(t,omega','Color', [ 0.8500 0.3250 0.0980], 'LineWidth', 2);
ylabel('PrÍdko∂ś (rad/s)');
subplot(3,1,3);
plot(t,alpha','Color', [ 0.8500 0.3250 0.0980], 'LineWidth', 2);
ylabel('Przyspieszenie (rad/s^2)');
xlabel('Czas (s)');