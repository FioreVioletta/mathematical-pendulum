function [A] = Accel(theta,omega,T,k,m,l,eps,w)
% Funkcja liczĪca przyspieszenie z wzoru
   g = 9.81; 
   A = -(g/l)*sin(theta) - ((k/m)*omega) + (eps/(m*l))*cos(w*sqrt(l/g)*T);

end

